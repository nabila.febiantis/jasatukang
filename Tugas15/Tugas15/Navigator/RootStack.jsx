import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { LoginScreen, RegisterScreen, SplashScreen } from "../Features/Auth";
import { Beranda, Profile, DaftarTukang, Detail } from "../Features/News";
import { useSelector } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";

const Stack = createNativeStackNavigator();
const tab = createBottomTabNavigator();

export const RootStack = () => {
	const isSignedIn = useSelector((state) => state.Auth.isSignedIn);

	return (
		<Stack.Navigator>
			{isSignedIn ? (
				<>
					<Stack.Screen name="Beranda" component={Beranda} />
					<Stack.Screen name="DaftarTukang" component={DaftarTukang} />
					<Stack.Screen name="Detail" component={Detail} />
					<Stack.Screen name="Profile" component={Profile} />
				</>
			) : (
				<>
					<Stack.Screen
						name="SplashScreen"
						component={SplashScreen}
						options={{ headerShown: false }}
					/>
					<Stack.Screen name="LoginScreen" component={LoginScreen} />
					<Stack.Screen name="RegisterScreen" component={RegisterScreen} />
				</>
			)}
		</Stack.Navigator>
	);
};

const styles = StyleSheet.create({});
