import { useFocusEffect } from "@react-navigation/native";
import {
	Alert,
	Button,
	FlatList,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	View,
	Image,
    WebView,
    HTML,
} from "react-native";
import { getListDetail } from "../../App";
import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { logout } from "../Auth/authSlice";

export const Detail = ({ route, navigation }) => {
	const dispatch = useDispatch();
    const { itemId } = route.params;
	const [title, setTitle] = useState("");
	const [avatar, setAvatar] = useState("");
	const [desc, setDesc] = useState("");
    const handleError = (err) => {
		console.warn("Error Status: ", err.response.data);
		Alert.alert("Gagal", err.response.data.message);
	};

    const getDetailApi = async () => {
		try {
            console.log('masuk sini');
            console.log('isi itemId : ',parseInt(itemId));
			const res = await getListDetail(parseInt(itemId));
			const _getDetail = res.data.data;
			console.log("res: ", _getDetail);
            setTitle(_getDetail.name);
            setAvatar(_getDetail.image);
            setDesc(_getDetail.description);
		} catch (error) {
			handleError(error);
		}
	};
    
    useFocusEffect(
		useCallback(() => {
			getDetailApi();
		}, []),
	);

	return (
		<View style={styles.container}>
			
			<View style={styles.form}>
                <Image
                    source={{uri:avatar}}
                    style={{width:200, height:300}}
                    />
                    <Text style={styles.newsTitle}>{title}</Text>
			</View>
                <Text style={styles.description}>{desc}</Text>
            <View style={styles.form}>
				<Button title="Keluar" color="#D02141" onPress={() => dispatch(logout())} />
			</View>
		</View>
        
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		paddingHorizontal: 20,
	},
	newsItem: {
		alignItems: "center",
		flexDirection: "row",
		marginVertical: 5,
	},
    avatar:{
        // position: 'absolute',
        width: 306,
        height: 152,
        // left: 37,
        // top: 73,
    },
	newsContent: {
		flex: 1,
		padding: 10,
		borderRadius: 12,
		borderColor: "grey",
		borderWidth: 1,
	},
	newsTitle: {
        fontSize:28,
		color: "black",
        fontWeight:"bold",
    },
	description: {
        fontSize:14,
		color: "black",
        fontWeight:"normal",
    },
	newsValue: {},
	deleteIcon: {
		borderWidth: 1,
		fontSize: 20,
		padding: 5,
		borderRadius: 12,
		color: "red",
	},
	form: {
		paddingVertical: 20,
        justifyContent: "center",
        alignItems: "center",
	},
	input: {
		borderWidth: 1,
		paddingVertical: 10,
		paddingHorizontal: 5,
		borderRadius: 6,
		marginBottom: 10,
	},
	contentNews: {
		backgroundColor: "grey",
		paddingVertical: 10,
	},
});
