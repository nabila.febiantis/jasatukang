import {
	Alert,
	Button,
	FlatList,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	View,
	Image,
} from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { logout } from "../Auth/authSlice";
import { DaftarTukang } from "./DaftarTukang";
import { Profile } from "./Profile";
import { backgroundColor } from "react-native/Libraries/Components/View/ReactNativeStyleAttributes";

const Tab = createBottomTabNavigator();

export const Beranda = ({ navigation }) => {
	const dispatch = useDispatch();

	return (
		<View style={styles.container}>
			
			<View style={styles.form}>
			<View style={styles.imageLogo3}>
				<Image style={styles.imageLogo} source={require('./logo.png')} />
			</View>
			<View style={styles.buttonClick}>
				<Button style={styles.buttonklik}
						title="Profile"
						onPress={() => {
							navigation.navigate("Profile");
						}}
					/>
			</View>
			<View style={styles.buttonClick}>
				<Button 
						title="Daftar Tukang"
						onPress={() => {
							navigation.navigate("DaftarTukang");
						}}
					/>
			</View>
			<View style={styles.buttonClose}>
				<Button title="Keluar" color="#D02141" onPress={() => dispatch(logout())} />
			</View>
			</View>
		</View>
	);
};

// const MainApp =()=>(
// 	<Tab.Navigator>
// 		<Tab.Screen name="DaftarTukang" component={DaftarTukang}></Tab.Screen>
// 	</Tab.Navigator>
// )

function MyTabs() {
	return (
	  <Tab.Navigator>
		<Tab.Screen name="DaftarTukang" component={DaftarTukang} />
		{/* <Tab.Screen name="Settings" component={SettingsScreen} /> */}
	  </Tab.Navigator>
	);
  }

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		paddingHorizontal: 20,
	},
	buttonClick: {
		marginBottom:10

	},
	buttonClose: {
		marginBottom:10,
		backgroundColor:"red",
	},
	imageLogo: {
		// position:"absolute",
		height:200,
		width:200,
		// left:115,
		// top:110,
		marginBottom:50,
		marginLeft:75,
		marginTop:100,
        // justifyContent: "center",
        // alignItems: "center",
	},
});
