import { useFocusEffect } from "@react-navigation/native";
import {
	Alert,
	Button,
	FlatList,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	View,
	Image,
} from "react-native";
import { getProfile } from "../../App";
import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { logout } from "../Auth/authSlice";

export const Profile = ({ navigation }) => {
	const dispatch = useDispatch();

	const [profiles, setProfile] = useState([]);
	const [title, setTitle] = useState("");
	const [avatar, setAvatar] = useState("");
	const [country, setCountry] = useState("");
	const [phone, setPhone] = useState("");
	const [email, setEmail] = useState("");
    const handleError = (err) => {
		console.warn("Error Status: ", err.response.data);
		Alert.alert("Gagal", err.response.data.message);
	};

    const getProfileApi = async () => {
		try {
            console.log('masuk sini');
			const res = await getProfile();
			const _getProfile = res.data.data.user;
			console.log("res: ", _getProfile);
			setProfile(_getProfile);
            setTitle(_getProfile.name);
            setAvatar(_getProfile.avatar);
            setCountry(_getProfile.country_name);
            setPhone(_getProfile.phone_number);
            setEmail(_getProfile.email);
		} catch (error) {
			handleError(error);
		}
	};
    
    useFocusEffect(
		useCallback(() => {
			getProfileApi();
		}, []),
	);

	return (
		<View style={styles.container}>
			
			<View style={styles.form}>
                <Image
                    source={{uri:avatar}}
                    style={{width:200, height:200}}
                    />
                    <Text style={styles.newsTitle}>{title}</Text>
                    <Text style={styles.newsTitle}>{country}</Text>
                    <Text style={styles.newsTitle}>{phone}</Text>
                    <Text style={styles.newsTitle}>{email}</Text>
                {/* <Text style={styles.newsValue}>{item.description}</Text> */}
				<Button title="Keluar" color="#D02141" onPress={() => dispatch(logout())} />
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		paddingHorizontal: 20,
	},
	newsItem: {
		alignItems: "center",
		flexDirection: "row",
		marginVertical: 5,
	},
    avatar:{
        // position: 'absolute',
        width: 306,
        height: 152,
        // left: 37,
        // top: 73,
    },
	newsContent: {
		flex: 1,
		padding: 10,
		borderRadius: 12,
		borderColor: "grey",
		borderWidth: 1,
	},
	newsTitle: {},
	newsValue: {},
	deleteIcon: {
		borderWidth: 1,
		fontSize: 20,
		padding: 5,
		borderRadius: 12,
		color: "red",
	},
	form: {
		paddingVertical: 20,
	},
	input: {
		borderWidth: 1,
		paddingVertical: 10,
		paddingHorizontal: 5,
		borderRadius: 6,
		marginBottom: 10,
	},
	contentNews: {
		backgroundColor: "grey",
		paddingVertical: 10,
	},
});
